<?php

namespace Database\Seeders;

use App\Models\CategoryItem;
use App\Models\Item;
use Database\Factories\CategoryItemFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoryItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryItem::factory()
            ->count(3)
            ->has(Item::factory()->count(30))
            ->create();
    }
}
