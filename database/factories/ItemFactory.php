<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Item::class;
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'commercial_name' => $this->faker->word(),
            'category_item_id' => $this->faker->numberBetween(1, 3),
        ];
    }
}
