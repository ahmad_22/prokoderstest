<?php

use App\Http\Controllers\CategoryItemController;
use App\Http\Controllers\ItemController;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('category')->controller(CategoryItemController::class)->group(function () {
    Route::post('create', 'create');
    Route::put('update/{category_item}', 'update');
    Route::delete('hide/{category_item}', 'softDelete');
    Route::get('all', 'all');
    Route::get('find/{category_item}', 'find');
});

Route::prefix('item')->controller(ItemController::class)->group(function () {
    Route::post('create', 'create');
    Route::put('update/{item}', 'update');
    Route::delete('hide/{item}', 'softDelete');
    Route::get('all', 'all');
    Route::get('find/{item}', 'find');
    Route::get('get', 'get');
});

