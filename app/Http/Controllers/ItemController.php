<?php

namespace App\Http\Controllers;

use App\Http\Requests\Item\CodeItemRequest;
use App\Http\Requests\Item\ItemDataRequest;
use App\Http\Resources\ItemResource;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function create(ItemDataRequest $request)
    {
        $data = Item::create($request->validated());
        return response()->json(['status' => 'OK', 'data' => ItemResource::make($data)], 200);
    }

    public function update(Item $item, ItemDataRequest $request)
    {
        $data = Item::where('id', $item->id)->update($request->validated());
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function softDelete(Item $item)
    {
        $data = Item::where('id', $item->id)->delete();
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function all()
    {
        $data = Item::with('category')->paginate();
        ItemResource::collection($data);
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function find(Item $item)
    {
        $item->load('category');
        return response()->json(['status' => 'OK', 'data' => ItemResource::make($item)], 200);
    }

    public function get(CodeItemRequest $request)
    {
        return response()->json(['status' => 'OK', 'data' => ItemResource::make((new Item())->extracts($request->code))], 200);
    }
}
