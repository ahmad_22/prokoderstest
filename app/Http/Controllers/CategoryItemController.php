<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryItem\CategoryItemDataRequest;
use App\Http\Requests\CategoryItem\CreateCategoryItemRequest;
use App\Http\Requests\CategoryItem\UpdateCategoryItemRequest;
use App\Http\Resources\CategoryItemResource;
use App\Models\CategoryItem;
use Illuminate\Http\Request;

class CategoryItemController extends Controller
{
    public function create(CategoryItemDataRequest $request)
    {
        $data = CategoryItem::create($request->validated());
        return response()->json(['status' => 'OK', 'data' => CategoryItemResource::make($data)], 200);
    }

    public function update(CategoryItem $categoryItem, CategoryItemDataRequest $request)
    {
        $data = CategoryItem::where('id', $categoryItem->id)->update($request->validated());
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function softDelete(CategoryItem $categoryItem)
    {
        $data = CategoryItem::where('id', $categoryItem->id)->delete();
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function all()
    {
        $data = CategoryItem::paginate();
        CategoryItemResource::collection($data);
        return response()->json(['status' => 'OK', 'data' => $data], 200);
    }

    public function find(CategoryItem $categoryItem)
    {
        return response()->json(['status' => 'OK', 'data' => CategoryItemResource::make($categoryItem)], 200);
    }
}
