<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'object' => 'item',
            'id' => $this->id,
            'name' => $this->name,
            'commercial_name' => $this->commercial_name,
            'create_date' => $this->created_at,
            'category_item_name' => $this->whenLoaded('category', $this->category->name),
        ];
    }
}
