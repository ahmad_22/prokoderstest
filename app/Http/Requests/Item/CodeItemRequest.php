<?php

namespace App\Http\Requests\Item;

use App\Traits\FormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CodeItemRequest extends FormRequest
{
    use FormRequestTrait;
    public function rules()
    {
        return [
            'code' => ['required', 'string', Rule::exists('items', 'code')
                ->whereNull('deleted_at')],
        ];
    }
}
