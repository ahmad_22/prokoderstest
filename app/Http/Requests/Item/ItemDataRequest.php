<?php

namespace App\Http\Requests\Item;

use App\Traits\FormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ItemDataRequest extends FormRequest
{
    use FormRequestTrait;
    public function rules()
    {
        return [
            'name' => ['required', 'max:30', 'min:4'],
            'commercial_name' => ['required', 'max:50'],
            'category_item_id' => ['required', 'integer', Rule::exists('category_items', 'id')
                ->whereNull('deleted_at')],
        ];
    }
}
