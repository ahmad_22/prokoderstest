<?php

namespace App\Http\Requests\CategoryItem;

use App\Traits\FormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class CategoryItemDataRequest extends FormRequest
{
    use FormRequestTrait;
    public function rules()
    {
        return [
            'name' => ['required', 'max:20'],
            'description' => ['nullable', 'max:100'],
        ];
    }
}
