<?php

namespace App\Console\Commands;

use App\Models\Item;
use Illuminate\Console\Command;

class generatedCodeItem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'auto-generated code refers to each item';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = Item::selectRaw('id, category_item_id ,left (name, 1) as first_char_name,
        left (commercial_name, 1) as first_char_commercial_name,
        length (commercial_name) as length_commercial_name')
            ->with([
                'category' => fn ($q) => $q->selectRaw('id,left (name, 1) as first_char_category_name,right (name, 1) as last_char_category_name')
            ])->get();

        foreach ($data as $item) {
            $code = $item['first_char_name'] . $item->category['first_char_category_name'] . $item->category['last_char_category_name'] . $item['first_char_commercial_name'];
            if ($item['length_commercial_name'] < 10)
                $code .= '00' . $item['length_commercial_name'];
            elseif ($item['length_commercial_name'] > 10 && $item['length_commercial_name'] < 99)
                $code .= '0' . $item['length_commercial_name'];
            else
                $code .= $item['length_commercial_name'];
            Item::where('id', $item['id'])->update(['code' => $code]);
        }
        return Command::SUCCESS;
    }
}

