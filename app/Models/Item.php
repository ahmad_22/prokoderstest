<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'items';

    protected $fillable = ['name', 'commercial_name', 'category_item_id'];

    protected $hidden = ['updated_at', 'deleted_at'];

    public function category()
    {
        return $this->belongsTo(CategoryItem::class, 'category_item_id');
    }

    public function extracts($code)
    {
        $items = $this->where('name', 'like', $code[0] . '%')
            ->where('commercial_name', 'like', $code[3] . '%')
            ->whereHas('category', function ($q) use ($code) {
                $q->where('name', 'like', $code[1] . '%' . $code[2]);
            })
            ->get();
        if ($items->count() > 1) {
            foreach ($items as $item) {
                if (strlen($item['commercial_name']) == (int)substr($code, 4))
                    return $item;
            }
        } else return $items->first();

        return $items;
    }
}
