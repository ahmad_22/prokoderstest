<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'category_items';

    protected $fillable = ['name', 'description'];

    protected $hidden = ['updated_at', 'deleted_at'];

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
