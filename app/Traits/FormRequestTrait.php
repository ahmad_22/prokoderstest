<?php


namespace App\Traits;

use App\Helpers\ValidateHelper;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

trait FormRequestTrait
{

    public function validationData()
    {
        return array_merge($this->all(), $this->route()->parameters());
    }

    public function authorize()
    {
        return true;
    }

    // protected function failedValidation(Validator $validator)
    // {
    //     if($this->expectsJson())
    //     {
    //         $validateHelper = new ValidateHelper($validator->errors());
    //         throw new HttpResponseException($validateHelper->checkErrors());
    //     }
    //     parent::failedValidation($validator);
    // }
}
